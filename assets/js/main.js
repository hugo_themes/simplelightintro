'use strict';

window.jQuery = window.$ = require('jquery');
const anime = require('animejs');

$( document ).ready(function() {
    // Variables
    const scrollElement = window.document.scrollingElement || window.document.body || window.document.documentElement;
    var returnTopBtnCs = '.return-page-top';
    var navBar = $('.nav-bar');
    var hamburger = $('.hamburger');

    var setAriaExpandedNavbar = function() {
        var windowWidth = $(window).width() / parseFloat($("body").css("font-size"));
        navBar.attr('aria-hidden', navBar.is(":hidden"));
        hamburger.attr('aria-hidden', hamburger.is(":hidden"));

        if (windowWidth < 62) {
            hamburger.attr('aria-expanded', navBar.is(":visible"));
        } else {
            hamburger.removeAttr('aria-expanded');
        }
    }

    // Hamburger menu on mobile
    hamburger.click(function(){
        $(this).toggleClass('hamburger-change');

        // With a fix to avoid losing the navbar when resising the screen...
        var setNavBarVisibility = function() {
            navBar.css('display', '');
            navBar.toggleClass('hide-on-mobile');
            hamburger.attr('aria-expanded', navBar.is(":visible"));
            navBar.attr('aria-hidden', navBar.is(":hidden"));
        }

        if (navBar.is(":hidden")) {
            navBar.slideDown(400, setNavBarVisibility);
        } else {
            navBar.slideUp(400, setNavBarVisibility);
        }
    });

    $(window).on('resize', setAriaExpandedNavbar);
    setAriaExpandedNavbar();

    // Scroll event
    $(window).on('scroll', function(){
        // Return to top button
        var showAfter = 100;
        if ( $(this).scrollTop() > showAfter ) {
         $(returnTopBtnCs).fadeIn();
        } else {
         $(returnTopBtnCs).fadeOut();
        }
    });
    window.dispatchEvent(new CustomEvent('scroll'));

    // Return page top
    $(returnTopBtnCs).click(function(){
        window.location.hash = '';
        anime({
            targets: scrollElement,
            scrollTop: 0,
            duration: 500,
            easing: 'easeInOutQuad'
        });
    });
});