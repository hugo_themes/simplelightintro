'use strict';

window.jQuery = window.$ = require('jquery');
const anime = require('animejs');

import 'jquery-validation';

$( document ).ready(function() {
    // Variables
    const scrollElement = window.document.scrollingElement || window.document.body || window.document.documentElement;

    var contactImageId = '#contact-image';
    var servicesBlocksRowCs = '.services-blocks-row';
    var serviceCardCs = '.service-card';
    var wordCloudColCs = '.word-cloud-col';
    var techLogoCs = '.tech-logo';

    var contactForm = $('#contact-form');
    var contactFormButton = $('#contact-form > button');

    $(".sliding-link").click(function(e) {
        e.preventDefault();
        var hash = '#' + $(this).attr("href").split('#')[1];
        window.location.hash = hash;
        anime({
            targets: scrollElement,
            scrollTop: $(hash).offset().top,
            duration: 500,
            easing: 'easeInOutQuad'
        });
    });

    // Service blocks animation
    $(window).on('scroll', function(){
        // Service cards
        $(serviceCardCs).each(function() {
            if ($(this).offset().top <= $(window).scrollTop() + $(window).height() * 0.75) {
                if ($(this).css('opacity') == 0) {
                    anime({
                        targets: this,
                        delay: anime.stagger(400),
                        opacity: [0, 1],
                        scale: [0.5, 1],
                        duration: 800,
                        easing: "linear",
                    });
                }
            }
        });

        // Service blocks
        $(servicesBlocksRowCs).each(function() {
            if ($(this).offset().top <= $(window).scrollTop() + $(window).height() * 0.75) {
                let block = $(this).children('.service-block').first()
                let icon = $(this).children('.services-blocks-icon').first()

                let translate = -200;
                if ($(this).hasClass('services-blocks-row-odd')) {
                    translate = 200;
                }

                if (block.css('opacity') == 0) {
                    anime({
                        targets: block.get(0),
                        translateX: [translate, 0],
                        opacity: [0, 1],
                        duration: 1000,
                        easing: "linear",
                    });
                }

                if (icon.css('opacity') == 0) {
                    anime({
                        targets: icon.get(0),
                        translateX: [translate * -1, 0],
                        opacity: [0, 1],
                        duration: 1000,
                        easing: "linear",
                    });
                }
            }
        });

        // Word clouds section columns
        if ($(wordCloudColCs).offset().top <= $(window).scrollTop() + $(window).height() * 0.75) {
            if ($(wordCloudColCs).css('opacity') == 0) {
                anime({
                    targets: wordCloudColCs,
                    opacity: [0, 1],
                    keyframes: [
                        {translateX: anime.stagger([-300, 300]), duration: 0},
                        {translateX: anime.stagger([0, 0]), duration: 2000},
                    ],
                    easing: 'easeInOutBack',
                });
            }
        }
    });
    window.dispatchEvent(new CustomEvent('scroll'));

    // Email contact image
    var animeContactImageHover = anime({
        targets: contactImageId,
        rotate: -10,
        duration: 500,
        easing: 'easeInOutBack',
        autoplay: false,
    });

    function rotateContactImageIn() {
        if (animeContactImageHover.reversed) {
            animeContactImageHover.reverse();
        }
        animeContactImageHover.play();
    }

    function rotateContactImageOut() {
        if (!animeContactImageHover.reversed) {
            animeContactImageHover.reverse();
        }
        animeContactImageHover.play();
    }

    $(contactImageId).hover(rotateContactImageIn, rotateContactImageOut);

    // Tech logos
    $(techLogoCs).hover(function() {
        anime ({
            targets: this,
            scale: 1.2,
            duration: 500,
            easing: 'easeInOutBack',
    });
    }, function() {
        anime ({
            targets: this,
            scale: 1,
            duration: 500,
            easing: 'easeInOutBack',
        });
    });

    // Contact form
    contactForm.submit(function(event){
        event.preventDefault();

        $.validator.methods.email = function( value, element ) {
            return this.optional( element ) || /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test( value );
        }

        contactForm.validate({
            errorClass:'form-error',
            errorElement:'span',
            rules: {
                name: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                subject: {
                    required: true,
                    minlength: 2
                },
                message: {
                    required: true,
                    minlength: 10
                },
            },
            highlight: function(element, errorClass, validClass) {
                $(element).attr('aria-invalid', 'true');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).attr('aria-invalid', 'false');
                $(element).removeAttr('aria-describedby');
            },
            messages: contactForm.data('jquery-validation-messages')
        });

        $.ajax({
            url: contactForm.attr('action'),
            type: 'POST',
            data : contactForm.serialize(),
            beforeSend: function() {
                if (contactForm.valid()) {
                    contactFormButton.html(contactFormButton.data('submit-message-sending'));
                    contactFormButton.attr('disabled', 'disabled');
                }

                return contactForm.valid();
            },
            success: function(response){
                contactFormButton.html(contactFormButton.data('submit-message-sent'));
            },
            error: function(response){
                contactFormButton.html(contactFormButton.data('submit-message-error'));
            },
        });
    });
});