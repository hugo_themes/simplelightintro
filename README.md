# Simple and Light Intro

It is not perfect, but I'm very open to any suggestions, comments, pull requests, etc. Otherwise...enjoy!

Demo site : https://hugo_themes.gitlab.io/simplelightintro/

## Setup

 - Clone the repo into the themes/ directory of your site
 ```
 git submodule add https://gitlab.com/gilauk/SimpleLightIntro themes/SimpleLightIntro
 ```
 - Install the dependencies (tested with node 13.14 /w npm 6.14)
 ```
 hugo mod npm pack
 npm install
 ```
 - Copy the sample config from exampleSite/config.yaml and edit its content
 - Copy the sample data from exampleSite/data and edit
 - If you wish to override the default colors, copy the file themes/SimpleLightIntro/assets/css/overrides/variables.scss to assets/css/overrides/variables.scss and edit its content
 - Copy themes/SimpleLightIntro/static/scripts/contact.php to static/scripts/contact.php and change the top variables to your liking
 - Add svg icons in static/images or any path you specified in the data files. I personnally use SVGs from https://feathericons.com/
 - Build the site with Hugo as usual
 - Here's the config I use with Nginx and PHP-FPM for the contact script to work :
 ```
location /scripts {
    try_files $uri $uri/ @extensionless-php;
    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass   unix:/var/run/php-fpm.sock;
        fastcgi_index  index.php;
        include        fastcgi_params;
        fastcgi_param  SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param  HTTPS $https;
    }
}

location @extensionless-php {
    rewrite ^(.*)$ $1.php last;
}
 ```

## Features

 - Tested on Hugo v0.83.1+extended
 - The theme works **even if Javascript is disabled** in the browser (not the contact form though, for now...)
 - Fully accessible as far as I know. Missing some aria attributes tough.
 - PHP script if you want the form to work propertly
 - Cool animations with AnimeJS
